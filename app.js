const express = require('express')
const app = express()
const port = 3000
const path = require('path');
const router = express.Router();
var http = require('http').Server(app);
var io = require('socket.io')(http);
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

router.get('/',function(req,res){
    res.sendFile(path.join(__dirname+'/index.html'));
    io.emit('message'," req.body");
    
});

router.get('/control',function(req,res){
    res.sendFile(path.join(__dirname+'/control.html'));
});

router.get('/test',function(req,res){
    io.emit('message'," req.body");
});



router.get('/style',function(req,res){
    res.sendFile(path.join(__dirname+'/style.css'));
});

app.use(express.static(__dirname + '/assets'));


io.on('connection', function (socket) {
    socket.on('my other event', function (data) {
        io.emit('update',data);
    });
    socket.on('team_name', function (data) {
        io.emit('team_name',data);
    });
  });

app.use('/', router);
http.listen(port, () => console.log(`Example app listening on port ${port}!`))